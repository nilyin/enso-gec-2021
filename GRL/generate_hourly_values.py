#!/usr/bin/python3
"""

Sarafanov F.G., Shatalina M.V. (C) IAP RAS, 2021

Calculate hourly mean of electric field from Vostok observations (10s)
For run script need have files:
    1) script_folder/efm_data/some_year/VOSEF*.log,
        files, where contains 10 sec - mean electric field meter (EFM) measurements
    2) script_folder/calibrations_coeffs/coeff.tsv,
        file, where contains monthly calibration coefficients
    3) cspans.py, python script, where contains time intervals during which 
        the electric field meter was calibrated.

"""

import glob
import sys
from multiprocessing import Pool
import pandas as pd
from cspans import *

def read_file_10s(file_name):
    """
    Read single comma-separated EFM *.log file
    to Pandas DataFrame
    """
    print(file_name, file=sys.stderr)
    data = pd.read_csv(file_name,
                       sep=',',
                       names=['DOY', 'Datetime', 'RPM', 'Cts', 'Cal', 'Temp'],
                       usecols=['Datetime', 'Cts', 'Temp'],
                       header=None,
                       comment=';',
                       engine='c',
                       skip_blank_lines=True)
    data['Datetime'] = pd.to_datetime(data['Datetime'],
                                      format='%Y-%m-%d %H:%M:%S')
    data['Cts'] = data["Cts"].astype(int)
    return data


if __name__ == '__main__':
    """
    Read calibration coefficients from file, parse datetime index
    and resample calibration to 1-hour with linear approximation.

    If there is no calibration, the `coeff.tsv` must contain two lines: 

    1990-01-01 00:00:00    1    0    1    0
    2030-01-01 00:00:00    1    0    1    0

    In this case, applying the calibration file will not break the data.

    """
    cal = pd.read_csv('./calibrations_coeffs/coeff.tsv',
                    sep='\t',
                    names=['Datetime', 'a_down', 'b_down', 'a_up', 'b_up'],
                    header=None,
                    comment=';',
                    engine='c')

    cal['Datetime'] = pd.to_datetime(cal['Datetime'], format='%Y-%m-%d %H:%M:%S')
    cal = cal.set_index("Datetime")
    cal = cal.resample('1H').\
      interpolate(method='linear',
                  limit_direction='forward',
                  axis=0)

    """
    Get 10-sec mean values from multiple files
    """
    files = [fn for fn in glob.glob('./efm_data/**/VOSEF*.log')]


    # Multiprocessing data conversion, merging data from separate files into a single table
    with Pool(processes=16) as pool:
        df = pd.concat(pool.map(read_file_10s, files), axis=0, ignore_index=True)
        df = df.sort_values(by='Datetime')


        # Deleting the time intervals during which the electric field meter was calibrated 
        for time_span in ts:
            print(time_span)
            df = df[(df["Datetime"] >= time_span[1]) | (df["Datetime"] <= time_span[0])]

        df = df.set_index("Datetime")
        df = df.loc[~df.index.duplicated(keep="first")]


        # Counting the number of samples per hour
        cdf = df[["Cts"]].\
          groupby(pd.Grouper(freq="H")).\
          count().\
          rename(columns={"Cts": "Count"})

        df = df.groupby(pd.Grouper(freq="H")).mean()


    # Combining raw data, calibration coefficients and information about the number of samples per hour
    df = pd.concat([df, cal, cdf], axis=1, join='inner')

    # Using calibrations
    df["Field"] = 0.5*(df["a_down"]*df["Cts"]/10000 + df["b_down"] +
                     df["a_up"]*df["Cts"]/10000 + df["b_up"])


    # If the hour has less than 80% of counts, delete it
    # 1 hour = 3600 seconds, 1 sample = 10 second => 1 hour = 360 samples
    df = df[df["Count"] >= 360/100*80][["Field"]]


    # Data output
    df = df.resample('1H').asfreq()
    df = df.reset_index()
    df.to_csv("./../vostok_hourly_from_10s_all_calibration_with_empty.tsv",
            sep='\t',
            index=None,
            float_format="%0.4f",
            date_format="%Y-%m-%d %H:%M:%S")