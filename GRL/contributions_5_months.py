# a script for plotting preliminary versions of Fig. 3 and Fig. 4
# takes ionospheric potential values from FULL-IP.npy
# plots Fig. 3 as contributions_year_to_year_removed_5_months.eps
# plots Fig. 4 as vertical_velocity_anomalies_5_months.eps

from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

Y = 11  # number of years considered

SSTs = [0.76, -1.56, -0.65, 1.39, -1.51, -0.95, -0.15, -0.30, 0.59, 2.53]
# SST anomalies for Oct–Feb periods of 2006/07–2015/16
# processed data from https://psl.noaa.gov/data/correlation/nina34.data

posidx = [i for i in range(Y - 1) if SSTs[i] > 0]
negidx = [i for i in range(Y - 1) if SSTs[i] < 0]
# numbers of years with positive and negative SST anomalies

posidx2 = [i for i in range(Y - 1) if SSTs[i] >= 0.9]
negidx2 = [i for i in range(Y - 1) if SSTs[i] <= -0.9]
# numbers of years with strong El Niños and La Niñas

dataIP = np.load('FULL-IP.npy') # contains ionospheric potential values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 4991
# every third day is taken, 0 corresponds to 1 Jan 1980 and 4991 corresponds to 29 Dec 2020
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])
# all values are zeros except for those corresponding to Oct–Feb periods in winter seasons of 2006/07–2015/16
# (the 40-year timespan is used for compatibility with other scripts not related to this study)

IPsums = np.zeros((Y - 1, 25, 5))       # sums of the ionospheric potential values for different years and regions
IPNs = np.zeros((Y - 1))                # number of days available for different years
# we consider 5-month Oct–Feb periods in winter seasons of 2006/07–2015/16

reg = {8: 0, 9: 0, 3: 1, 13: 1, 4: 2, 5: 2, 6: 2, 7: 2, 1: 3, 2: 3, 10: 4, 11: 4, 12: 4}
# the combinations of regions considered

for i in range(dataIP.shape[0]):
    currdate = dt.date(1980, 1, 1) + dt.timedelta(i * 3)
    if (currdate.month >= 10) and (currdate.year >= 2006) and (currdate.year <= 2006 + Y - 2):
        y = currdate.year - 2006
    elif (currdate.month <= 2) and (currdate.year >= 2007) and (currdate.year <= 2006 + Y - 1):
        y = currdate.year - 2007
    else:
        continue
    for r in range(1, 14):
        IPsums[y, :, reg[r]] += dataIP[i, r, :]
    IPNs[y] += 1

yIPsums = IPsums[:, :, :] /\
    (np.mean(np.sum(IPsums[:, :-1, :, np.newaxis, np.newaxis], axis = 2), axis = 1) /\
    IPNs[:, np.newaxis, np.newaxis])
# sums of the ionospheric potential values for different years and regions with year-to-year variability removed

totIPN = np.sum(IPNs[:])
posIPN = np.sum(IPNs[np.ix_(posidx)])
negIPN = np.sum(IPNs[np.ix_(negidx)])
posIPN2 = np.sum(IPNs[np.ix_(posidx2)])
negIPN2 = np.sum(IPNs[np.ix_(negidx2)])
# numbers of days

yIPs = yIPsums[:, :, :] / IPNs[:, np.newaxis, np.newaxis]
totyIPs = np.sum(yIPsums[:, :, :], axis = 0) / totIPN
posyIPs = np.sum(yIPsums[np.ix_(posidx, range(25), range(5))], axis = 0) / posIPN
negyIPs = np.sum(yIPsums[np.ix_(negidx, range(25), range(5))], axis = 0) / negIPN
posyIPs2 = np.sum(yIPsums[np.ix_(posidx2, range(25), range(5))], axis = 0) / posIPN2
negyIPs2 = np.sum(yIPsums[np.ix_(negidx2, range(25), range(5))], axis = 0) / negIPN2
# values of variables for all years with positive/negative anomalies and strong El Niños/La Niñas combined

col = np.array([[64, 64, 191], [0, 191, 255], [159, 191, 64], [255, 255, 128], [255, 128, 0]]) / 255.
# colours of the plots
order = np.array([0, 1, 2, 3, 4]) # order of the plots

nw, nh = 2, 2   # number of subplots in horizontal (width) and vertical (height) directions
w = 3.3         # width of each subplot
wt = 0.4        # width of tick labels
wl = 0.5        # width of each axis label
ws = 0.4        # horizontal spacing between subplots
h = [3.0, (2 * w + ws + wt) / 2]
# heights of subplots in different rows
assert len(h) == nh
ht = 0.2        # height of tick labels
hl = 0.3        # height of each axis label
hs = 0.2        # vertical spacing between subplots
hu = 0.2        # height of each subplot label
wm = 0.2        # width of margins
tw = nw * (w + wt) + wl + (nw - 1) * ws + 2 * wm        # total width
hm = 0.2        # height of margins
th = sum(h[i] for i in range(nh)) + nh * (ht + hu) + hl + (nh - 1) * hs + 2 * hm        # total height
fig = plt.figure(figsize = (tw, th))
ax = [None for _ in range(nw * nh)]
for n in range(nw * (nh - 1)):
    i, j = divmod(n, nw)
    ax[n] = fig.add_axes([(wm + wl + j * (w + ws) + (j + 1) * wt) / tw,\
        (hm + (hl if (i < nh - 1) else 0) + sum(h[nh - 1 - i] for i in range(nh - 1 - i)) +\
        (nh - 1 - i) * (hu + hs) + (nh - i) * ht) / th,\
        w / tw, h[i] / th])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    ax[n].set_xlim((0, 24))
    ax[n].set_xticks(np.arange(0, 25, 2))
    ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
    ax[n].set_xlabel('UTC, hours', fontsize = 'large')
    ax[n].get_yaxis().set_label_coords(-0.25, 0.5)
    ax[n].set_ylim((0., 1.2))
    ax[n].set_yticks(np.arange(0., 1.21, 0.2))
    ax[n].set_yticklabels(['0.0', '0.2', '0.4', '0.6', '0.8', '1.0', '1.2'], fontsize = 'large')
    if j == 0:
        ax[n].set_ylabel('Ion. potential in Oct–Feb\nas a fraction of the mean', fontsize = 'large', va = 'top')
    if n == 0:
        for k in range(5, 0, -1):
            ax[n].fill_between(np.arange(25),\
                0, sum(posyIPs2[:, r] for r in order[:k]), color = col[order[k - 1]])
        for k in range(5, 1, -1):
            ax[n].plot(np.arange(25), sum(negyIPs2[:, r] for r in order[:k]),\
                linestyle = 'dashed', clip_on = False, linewidth = 1.25, color = '0.', zorder = 1.5)
        for k in range(1, 0, -1):
            ax[n].plot(np.arange(25), sum(negyIPs2[:, r] for r in order[:k]),\
                linestyle = 'dashed', clip_on = False, linewidth = 1.25, color = '1.', zorder = 1.5)
        ax[n].set_title('Model, strong El Niños', fontsize = 'large')
    else:
        for k in range(5, 0, -1):
            ax[n].fill_between(np.arange(25),\
                0, sum(negyIPs2[:, r] for r in order[:k]), color = col[order[k - 1]])
        for k in range(5, 0, -1):
            ax[n].plot(np.arange(25), sum(posyIPs2[:, r] for r in order[:k]),\
                linestyle = 'dashed', clip_on = False, linewidth = 1.25, color = '0.', zorder = 1.5)
        ax[n].set_title('Model, strong La Niñas', fontsize = 'large')
    if j == 0:
        ax[n].text(-0.27, 1 + 0.07 * 1.6 / h[i], chr(ord('a') + n), fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
    else:
        ax[n].text(-0.17, 1 + 0.07 * 1.6 / h[i], chr(ord('a') + n), fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
ax[-1] = fig.add_axes([(wm + wl + wt) / tw, (hm + ht) / th, 2 * h[1] / tw, h[1] / th])
ax[-1].spines['top'].set_linewidth(0.5)
ax[-1].spines['bottom'].set_linewidth(0.5)
ax[-1].spines['left'].set_linewidth(0.5)
ax[-1].spines['right'].set_linewidth(0.5)
ax[-1].grid(False)
ax[-1].tick_params(top = True, right = True, which = 'both')
ax[-1].tick_params(length = 6, width = 0.5)
ax[-1].tick_params(length = 3, width = 0.5, which = 'minor')
ax[-1].set_xlim(-180, 180)
ax[-1].set_ylim(-90, 90)
ax[-1].set_xticks(np.arange(-180, 181, 30))
ax[-1].set_xticks(np.arange(-180, 181, 10), minor = True)
ax[-1].set_yticks(np.arange(-90, 91, 30))
ax[-1].set_yticks(np.arange(-90, 91, 10), minor = True)
ax[-1].set_xticklabels(['180', '150W', '120W', '90W', '60W', '30W', '0', '30E', '60E', '90E', '120E', '150E', '180'],\
    fontsize = 'large')
ax[-1].set_yticklabels(['90S', '60S', '30S', '0', '30N', '60N', '90N'], fontsize = 'large')
ax[-1].set_title('Partition of the Earth’s surface into regions', fontsize = 'large', pad = 10.)
ax[-1].text(-0.18 * w / (2 * h[1]), 1 + 0.07 * 1.6 / h[1], chr(ord('a') + 2), fontsize = 'x-large',\
    fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[-1].transAxes)

plt.savefig('contributions_year_to_year_removed_5_months.eps', bbox_inches = 'tight')

nw, nh = 2, 3   # number of subplots in horizontal (width) and vertical (height) directions
w = 3.3         # width of each subplot
wt = 0.4        # width of tick labels
ws = 0.4        # horizontal spacing between subplots
h = [(2 * w + ws + wt) / 2, (2 * w + ws + wt) / 2, (2 * w + ws + wt) / 30]
# heights of subplots in different rows
assert len(h) == nh
ht = 0.2        # height of tick labels
hl = 0.3        # height of each axis label
hs = 0.3        # vertical spacing between subplots
hu = 0.2        # height of each subplot label
wm = 0.2        # width of margins
tw = nw * (w + wt) + (nw - 1) * ws + 2 * wm        # total width
hm = 0.2        # height of margins
th = sum(h[i] for i in range(nh)) + nh * (ht + hu) + hl + (nh - 1) * hs + 2 * hm        # total height
fig = plt.figure(figsize = (tw, th))
ax = [None for _ in range(nw * nh)]
ax[0] = fig.add_axes([(wm + wt) / tw, (hm + hl + h[1] + h[2] + hu + 2 * hs + 3 * ht) / th, 2 * h[0] / tw, h[0] / th])
ax[0].spines['top'].set_linewidth(0.5)
ax[0].spines['bottom'].set_linewidth(0.5)
ax[0].spines['left'].set_linewidth(0.5)
ax[0].spines['right'].set_linewidth(0.5)
ax[0].grid(False)
ax[0].tick_params(top = True, right = True, which = 'both')
ax[0].tick_params(length = 6, width = 0.5)
ax[0].tick_params(length = 3, width = 0.5, which = 'minor')
ax[0].set_xlim(-180, 180)
ax[0].set_ylim(-90, 90)
ax[0].set_xticks(np.arange(-180, 181, 30))
ax[0].set_xticks(np.arange(-180, 181, 10), minor = True)
ax[0].set_yticks(np.arange(-90, 91, 30))
ax[0].set_yticks(np.arange(-90, 91, 10), minor = True)
ax[0].set_xticklabels(['180', '150W', '120W', '90W', '60W', '30W', '0', '30E', '60E', '90E', '120E', '150E', '180'],\
    fontsize = 'large')
ax[0].set_yticklabels(['90S', '60S', '30S', '0', '30N', '60N', '90N'], fontsize = 'large')
ax[0].set_title('Oct–Feb, strong El Niños', fontsize = 'large', pad = 10.)
ax[0].text(-0.18 * w / (2 * h[0]), 1 + 0.07 * 1.6 / h[0], 'a', fontsize = 'x-large',\
    fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[0].transAxes)
ax[1] = fig.add_axes([(wm + wt) / tw, (hm + hl + h[2] + hs + 2 * ht) / th, 2 * h[1] / tw, h[1] / th])
ax[1].spines['top'].set_linewidth(0.5)
ax[1].spines['bottom'].set_linewidth(0.5)
ax[1].spines['left'].set_linewidth(0.5)
ax[1].spines['right'].set_linewidth(0.5)
ax[1].grid(False)
ax[1].tick_params(top = True, right = True, which = 'both')
ax[1].tick_params(length = 6, width = 0.5)
ax[1].tick_params(length = 3, width = 0.5, which = 'minor')
ax[1].set_xlim(-180, 180)
ax[1].set_ylim(-90, 90)
ax[1].set_xticks(np.arange(-180, 181, 30))
ax[1].set_xticks(np.arange(-180, 181, 10), minor = True)
ax[1].set_yticks(np.arange(-90, 91, 30))
ax[1].set_yticks(np.arange(-90, 91, 10), minor = True)
ax[1].set_xticklabels(['180', '150W', '120W', '90W', '60W', '30W', '0', '30E', '60E', '90E', '120E', '150E', '180'],\
    fontsize = 'large')
ax[1].set_yticklabels(['90S', '60S', '30S', '0', '30N', '60N', '90N'], fontsize = 'large')
ax[1].set_title('Oct–Feb, strong La Niñas', fontsize = 'large', pad = 10.)
ax[1].text(-0.18 * w / (2 * h[1]), 1 + 0.07 * 1.6 / h[1], 'b', fontsize = 'x-large',\
    fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[1].transAxes)
ax[2] = fig.add_axes([(wm + wt) / tw, (hm + hl + ht) / th, 30 * h[2] / tw, h[2] / th])
ax[2].spines['top'].set_linewidth(0.5)
ax[2].spines['bottom'].set_linewidth(0.5)
ax[2].spines['left'].set_linewidth(0.5)
ax[2].spines['right'].set_linewidth(0.5)
ax[2].grid(False)
ax[2].tick_params(length = 0, width = 0.5)
ax[2].set_xlim(0, 10)
ax[2].set_xticks(np.arange(0, 11, 1))
ax[2].set_xticklabels(['min', '−8', '−4', '−2', '−1', '0', '+1', '+2', '+4', '+8', 'max'],\
    fontsize = 'large')
ax[2].set_xlabel('Anomaly of the mean vertical velocity around the 500-mbar level, mm/s', fontsize = 'large')
ax[2].set_ylim(0, 1)
ax[2].set_yticks([])
ax[2].set_yticklabels([])

plt.savefig('vertical_velocity_anomalies_5_months.eps', bbox_inches = 'tight')
