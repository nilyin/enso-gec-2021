# an auxiliary script for plotting vertical velocity anomalies
# takes velocity values from W_El_Ninos.bin and W_La_Ninas.bin
# plots the data as bitmaps to W_El_Ninos.png and W_La_Ninas.png
# also plots the colour bar

from sys import stdin, stdout, stderr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

ENdata = np.roll(np.flip(np.fromfile('W_El_Ninos.bin', dtype = float).\
    reshape((180, 360)), axis = 0), shift = 180, axis = 1)
LNdata = np.roll(np.flip(np.fromfile('W_La_Ninas.bin', dtype = float).\
    reshape((180, 360)), axis = 0), shift = 180, axis = 1)

bounds = np.array([-0.008, -0.004, -0.002, -0.001, 0., 0.001, 0.002, 0.004, 0.008])
cols = np.array([[87, 0, 136], [29, 0, 215], [3, 60, 175], [8, 179, 15], [173, 230, 175],\
    [255, 255, 170], [255, 255, 0], [255, 195, 0], [255, 110, 0], [255, 0, 0]]) / 255.

cmap = (mpl.colors.ListedColormap(cols[1:-1]).\
    with_extremes(over = cols[-1], under = cols[0]))
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

plt.imsave('W_El_Ninos.png', cmap(norm(ENdata)))
plt.imsave('W_La_Ninas.png', cmap(norm(LNdata)))

fig, ax = plt.subplots(figsize = (6, 1))

bounds = np.append(np.insert(bounds, 0, -1), 1)
cmap = mpl.colors.ListedColormap(cols)
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
fig.colorbar(mpl.cm.ScalarMappable(norm = norm, cmap = cmap),\
    cax = ax, orientation = 'horizontal', ticks = None, drawedges = False)

plt.savefig('color_bar.eps', bbox_inches = 'tight')
