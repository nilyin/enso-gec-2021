# a script for computing Niño 3.4 SST anomalies
# takes SST values from nino34.txt
# yields the anomalies

from sys import stdin, stdout, stderr
import numpy as np
from calendar import monthrange

Y = np.full((31), np.nan, dtype = int)      # years
T = np.full((31, 12), np.nan)               # monthly temperatures

with open('nino34.txt') as f:
    s = 0
    for line in f:
        l = line.strip().split()
        Y[s] = int(l[0])
        T[s] = list(map(float, l[1:]))
        s += 1
# the data are from https://psl.noaa.gov/data/correlation/nina34.data
# see also https://psl.noaa.gov/data/climateindices/list

Tsums = np.zeros((30))
Ns = np.zeros((30), dtype = int)
for s in range(30):
    for m in range(10, 13):
        Tsums[s] += monthrange(Y[s], m)[1] * T[s, m - 1]
        Ns[s] += monthrange(Y[s], m)[1]
    for m in range(1, 3):
        Tsums[s] += monthrange(Y[s + 1], m)[1] * T[s + 1, m - 1]
        Ns[s] += monthrange(Y[s + 1], m)[1]
# we consider Oct–Feb periods

meanTs = Tsums / Ns                         # means for individual years
globalmeanT = np.sum(Tsums) / np.sum(Ns)    # the 30-year mean

for s in range(30):
    stdout.write('{0:s}\t{1:.2f}\n'.\
        format(str(Y[s]) + '/' + str(Y[s] + 1)[2:], meanTs[s] - globalmeanT))
# anomalies