# a script for plotting preliminary versions of Fig. 1, Fig. 2 and Fig. S1
# takes potential gradient values from vostok_hourly_oct-feb.tsv
# takes ionospheric potential values from FULL-IP.npy
# plots Fig. 1 as ENSO_year_to_year_removed_5_months.eps
# plots Fig. 2 as selected_years_year_to_year_removed_5_months.eps
# plots Fig. S1 as all_years_year_to_year_removed_5_months.eps

from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

Y = 11  # number of years considered

SSTs = [0.76, -1.56, -0.65, 1.39, -1.51, -0.95, -0.15, -0.30, 0.59, 2.53]
# SST anomalies for Oct–Feb periods of 2006/07–2015/16
# processed data from https://psl.noaa.gov/data/correlation/nina34.data

posidx = [i for i in range(Y - 1) if SSTs[i] > 0]
negidx = [i for i in range(Y - 1) if SSTs[i] < 0]
# numbers of years with positive and negative SST anomalies

posidx2 = [i for i in range(Y - 1) if SSTs[i] >= 0.9]
negidx2 = [i for i in range(Y - 1) if SSTs[i] <= -0.9]
# numbers of years with strong El Niños and La Niñas

PGsums = np.zeros((Y - 1, 24))          # sums of the potential gradient values for different years
PGsqrsums = np.zeros((Y - 1, 24))       # sums of the squares of the potential gradient values for different years
relPGsums = np.zeros((Y - 1, 24))       # sums of the relative potential gradient values for different years
relPGsqrsums = np.zeros((Y - 1, 24))    # sums of the squares of the relative potential gradient values for different years
dPGsums = np.zeros((Y - 1, 24))         # sums of the potential gradient deviation values for different years
dPGsqrsums = np.zeros((Y - 1, 24))      # sums of the squares of the potential gradient deviation values for different years
PGNs = np.zeros((Y - 1), dtype = int)   # number of days available for different years
# we consider 5-month Oct–Feb periods in winter seasons of 2006/07–2015/16

PG = np.full((Y * 366, 24), np.nan) # all potential gradient values for consecutive hours

with open('vostok_hourly_oct-feb.tsv') as f:
    for line in f:
        s = line.strip().split()
        d = list(map(int, [s[0][:4], s[0][5:7], s[0][8:]]))
        if d[0] > 2016:
            break
        nd = (dt.datetime(d[0], d[1], d[2]) - dt.datetime(2006, 1, 1)).days
        h = int(s[1][:2])
        PG[nd, h] = float(s[2]) / 3.
    # loading the hourly data
    # potential gradient values are in V/m
    # for each day there are 24 hourly values corresponding to 0:00–1:00, 1:00–2:00 etc.

maxval = np.amax(PG, axis = 1)
minval = np.amin(PG, axis = 1)
diff = (maxval - minval) / np.maximum(np.mean(PG, axis = 1), 1)
# diurnal maxima, minima and peak-to-peak amplitudes relative to the diurnal mean

firstday = (dt.date(2006, 10, 1) - dt.date(2006, 1, 1)).days
lastday = (dt.date(2006 + Y - 1, 3, 1) - dt.date(2006, 1, 1)).days
for d in range(firstday, lastday):
    if np.isnan(maxval[d]):
        # the value 99999 corresponds to absent measurements
        continue
    if maxval[d] > 300.:
        # days with maximum values of the potential gradient exceeding 1 kV/m are omitted
        continue
    if minval[d] <= 0:
        # days with negative values of the potential gradient are omitted
        continue
    if diff[d] > 1.5:
        continue
        # days with large values of the relative diurnal peak-to-peak amplitude are omitted
    currdate = dt.date(2006, 1, 1) + dt.timedelta(d)
    if currdate.month >= 10:
        y = currdate.year - 2006
    elif currdate.month <= 2:
        y = currdate.year - 2007
    else:
        continue
    PGsums[y, :] += PG[d, :]
    PGsqrsums[y, :] += PG[d, :]**2
    relPGsums[y, :] += PG[d, :] / np.mean(PG[d, :], axis = 0)
    relPGsqrsums[y, :] += (PG[d, :] / np.mean(PG[d, :], axis = 0))**2
    dPGsums[y, :] += PG[d, :] - np.mean(PG[d, :], axis = 0)
    dPGsqrsums[y, :] += (PG[d, :] - np.mean(PG[d, :], axis = 0))**2
    PGNs[y] += 1

yPGsums = PGsums[:, :] /\
    (np.where(np.mean(PGsums[:, :, np.newaxis], axis = 1) != 0, np.mean(PGsums[:, :, np.newaxis], axis = 1), 1) /\
    np.maximum(PGNs[:, np.newaxis], 1))
# sums of the potential gradient values for different years with year-to-year variability removed
yPGsqrsums = PGsqrsums[:, :] /\
    (np.where(np.mean(PGsums[:, :, np.newaxis], axis = 1) != 0, np.mean(PGsums[:, :, np.newaxis], axis = 1), 1) /\
    np.maximum(PGNs[:, np.newaxis], 1))**2
# sums of the squares of the potential gradient values for different years with year-to-year variability removed

totPGN = np.sum(PGNs[:])
posPGN = np.sum(PGNs[np.ix_(posidx)])
negPGN = np.sum(PGNs[np.ix_(negidx)])
posPGN2 = np.sum(PGNs[np.ix_(posidx2)])
negPGN2 = np.sum(PGNs[np.ix_(negidx2)])
# numbers of days

yPGs = yPGsums[:, :] / np.maximum(PGNs[:, np.newaxis], 1)
totyPGs = np.sum(yPGsums[:, :], axis = 0) / totPGN
posyPGs = np.sum(yPGsums[np.ix_(posidx, range(24))], axis = 0) / posPGN
negyPGs = np.sum(yPGsums[np.ix_(negidx, range(24))], axis = 0) / negPGN
posyPGs2 = np.sum(yPGsums[np.ix_(posidx2, range(24))], axis = 0) / posPGN2
negyPGs2 = np.sum(yPGsums[np.ix_(negidx2, range(24))], axis = 0) / negPGN2
# values of variables for all years with positive/negative anomalies and strong El Niños/La Niñas combined

yPGsqrs = yPGsqrsums[:, :] / np.maximum(PGNs[:, np.newaxis], 1)
totyPGsqrs = np.sum(yPGsqrsums[:, :], axis = 0) / totPGN
posyPGsqrs = np.sum(yPGsqrsums[np.ix_(posidx, range(24))], axis = 0) / posPGN
negyPGsqrs = np.sum(yPGsqrsums[np.ix_(negidx, range(24))], axis = 0) / negPGN
posyPGsqrs2 = np.sum(yPGsqrsums[np.ix_(posidx2, range(24))], axis = 0) / posPGN2
negyPGsqrs2 = np.sum(yPGsqrsums[np.ix_(negidx2, range(24))], axis = 0) / negPGN2
yPGerrs = np.power((yPGsqrs[:, :] - yPGs[:, :]**2) / np.maximum(PGNs[:, np.newaxis] - 1, 1), 0.5)
totyPGerrs = np.power((totyPGsqrs[:] - totyPGs[:]**2) / (totPGN - 1), 0.5)
posyPGerrs = np.power((posyPGsqrs[:] - posyPGs[:]**2) / (posPGN - 1), 0.5)
negyPGerrs = np.power((negyPGsqrs[:] - negyPGs[:]**2) / (negPGN - 1), 0.5)
posyPGerrs2 = np.power((posyPGsqrs2[:] - posyPGs2[:]**2) / (posPGN2 - 1), 0.5)
negyPGerrs2 = np.power((negyPGsqrs2[:] - negyPGs2[:]**2) / (negPGN2 - 1), 0.5)
# standard errors of variables

dataIP = np.load('FULL-IP.npy') # contains ionospheric potential values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 4991
# every third day is taken, 0 corresponds to 1 Jan 1980 and 4991 corresponds to 29 Dec 2020
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])
# all values are zeros except for those corresponding to Oct–Feb periods in winter seasons of 2006/07–2015/16
# (the 40-year timespan is used for compatibility with other scripts)

IPsums = np.zeros((Y - 1, 25))          # sums of the ionospheric potential values for different years
IPsqrsums = np.zeros((Y - 1, 25))       # sums of the squares of the ionospheric potential values for different years
relIPsums = np.zeros((Y - 1, 25))       # sums of the relative ionospheric potential values for different years
relIPsqrsums = np.zeros((Y - 1, 25))    # sums of the squares of the relative ionospheric potential values for different years
dIPsums = np.zeros((Y - 1, 25))         # sums of the ionospheric potential deviation values for different years
dIPsqrsums = np.zeros((Y - 1, 25))      # sums of the squares of the ionospheric potential deviation values for different years
IPNs = np.zeros((Y - 1))                # number of days available for different years
# we consider 5-month Oct–Feb periods in winter seasons of 2006/07–2015/16

for i in range(dataIP.shape[0]):
    currdate = dt.date(1980, 1, 1) + dt.timedelta(i * 3)
    if (currdate.month >= 10) and (currdate.year >= 2006) and (currdate.year <= 2006 + Y - 2):
        y = currdate.year - 2006
    elif (currdate.month <= 2) and (currdate.year >= 2007) and (currdate.year <= 2006 + Y - 1):
        y = currdate.year - 2007
    else:
        continue
    IPsums[y, :] += dataIP[i, 0, :]
    IPsqrsums[y, :] += dataIP[i, 0, :]**2
    relIPsums[y, :] += dataIP[i, 0, :] / np.mean(dataIP[i, 0, :-1], axis = 0)
    relIPsqrsums[y, :] += (dataIP[i, 0, :] / np.mean(dataIP[i, 0, :-1], axis = 0))**2
    dIPsums[y, :] += dataIP[i, 0, :] - np.mean(dataIP[i, 0, :-1], axis = 0)
    dIPsqrsums[y, :] += (dataIP[i, 0, :] - np.mean(dataIP[i, 0, :-1], axis = 0))**2
    IPNs[y] += 1

yIPsums = IPsums[:, :] / (np.mean(IPsums[:, :-1, np.newaxis], axis = 1) / IPNs[:, np.newaxis])
# sums of the ionospheric potential values for different years with year-to-year variability removed
yIPsqrsums = IPsqrsums[:, :] / (np.mean(IPsums[:, :-1, np.newaxis], axis = 1) / IPNs[:, np.newaxis])**2
# sums of the squares of the ionospheric potential values for different years with year-to-year variability removed

totIPN = np.sum(IPNs[:])
posIPN = np.sum(IPNs[np.ix_(posidx)])
negIPN = np.sum(IPNs[np.ix_(negidx)])
posIPN2 = np.sum(IPNs[np.ix_(posidx2)])
negIPN2 = np.sum(IPNs[np.ix_(negidx2)])
# numbers of days

yIPs = yIPsums[:, :] / IPNs[:, np.newaxis]
totyIPs = np.sum(yIPsums[:, :], axis = 0) / totIPN
posyIPs = np.sum(yIPsums[np.ix_(posidx, range(25))], axis = 0) / posIPN
negyIPs = np.sum(yIPsums[np.ix_(negidx, range(25))], axis = 0) / negIPN
posyIPs2 = np.sum(yIPsums[np.ix_(posidx2, range(25))], axis = 0) / posIPN2
negyIPs2 = np.sum(yIPsums[np.ix_(negidx2, range(25))], axis = 0) / negIPN2
# values of variables for all years with positive/negative anomalies and strong El Niños/La Niñas combined

yIPsqrs = yIPsqrsums[:, :] / IPNs[:, np.newaxis]
totyIPsqrs = np.sum(yIPsqrsums[:, :], axis = 0) / totIPN
posyIPsqrs = np.sum(yIPsqrsums[np.ix_(posidx, range(25))], axis = 0) / posIPN
negyIPsqrs = np.sum(yIPsqrsums[np.ix_(negidx, range(25))], axis = 0) / negIPN
posyIPsqrs2 = np.sum(yIPsqrsums[np.ix_(posidx2, range(25))], axis = 0) / posIPN2
negyIPsqrs2 = np.sum(yIPsqrsums[np.ix_(negidx2, range(25))], axis = 0) / negIPN2
yIPerrs = np.power((yIPsqrs[:, :] - yIPs[:, :]**2) / (IPNs[:, np.newaxis] - 1), 0.5)
totyIPerrs = np.power((totyIPsqrs[:] - totyIPs[:]**2) / (totIPN - 1), 0.5)
posyIPerrs = np.power((posyIPsqrs[:] - posyIPs[:]**2) / (posIPN - 1), 0.5)
negyIPerrs = np.power((negyIPsqrs[:] - negyIPs[:]**2) / (negIPN - 1), 0.5)
posyIPerrs2 = np.power((posyIPsqrs2[:] - posyIPs2[:]**2) / (posIPN2 - 1), 0.5)
negyIPerrs2 = np.power((negyIPsqrs2[:] - negyIPs2[:]**2) / (negIPN2 - 1), 0.5)
# standard errors of variables

def sign(x):
    if x < 0:
        return '−'
    else:
        return '+'

nw, nh = 2, 5   # number of subplots in horizontal (width) and vertical (height) directions
w = 3.3         # width of each subplot
wt = 0.3        # width of tick labels
wl = 0.6        # width of each axis label
ws = 0.4        # horizontal spacing between subplots
h = [1.6, 1.6, 1.6, 1.6, 1.6]
# heights of subplots in different rows
assert len(h) == nh
ht = 0.2        # height of tick labels
hl = 0.2        # height of each axis label
hs = 0.2        # vertical spacing between subplots
hu = 0.2        # height of each subplot label
wm = 0.2        # width of margins
tw = nw * (w + wt) + wl + (nw - 1) * ws + 2 * wm        # total width
hm = 0.2        # height of margins
th = sum(h[i] for i in range(nh)) + nh * (ht + hu) + hl + (nh - 1) * hs + 2 * hm        # total height
fig = plt.figure(figsize = (tw, th))
ax = [None for _ in range(nw * nh)]
for n in range(nw * nh):
    i, j = divmod(n, nw)
    ax[n] = fig.add_axes([(wm + wl + j * (w + ws) + (j + 1) * wt) / tw,\
        (hm + hl + sum(h[nh - 1 - _] for _ in range(nh - 1 - i)) + (nh - 1 - i) * (hu + hs) + (nh - i) * ht) / th,\
        w / tw, h[i] / th])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    ax[n].set_xlim((0, 24))
    ax[n].set_xticks(np.arange(0, 25, 2))
    ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
    if ((i == nh - 1) and (j < 2)) or ((i == nh - 2) and (j == 2)):
        ax[n].set_xlabel('UTC, hours', fontsize = 'large')
    ax[n].get_yaxis().set_label_coords(-0.25, 0.5)
    ax[n].set_ylim((0.7, 1.3))
    ax[n].set_yticks(np.arange(0.7, 1.31, 0.1))
    ax[n].set_yticklabels(['0.7', '0.8', '0.9', '1.0', '1.1', '1.2', '1.3'], fontsize = 'large')
    if j == 0:
        ax[n].set_ylabel('Pot. grad. in Oct–Feb\nas a fraction of the mean', fontsize = 'large', va = 'top')
    xval = np.append(np.insert(np.arange(0.5, 24, 1), 0, 0), 24)
    PG1bound = (totyPGs[0] + totyPGs[-1]) / 2
    PG2bound = (yPGs[n, 0] + yPGs[n, -1]) / 2
    PG1errbound = (totyPGerrs[0] + totyPGerrs[-1]) / 2
    PG2errbound = (yPGerrs[n, 0] + yPGerrs[n, -1]) / 2
    yPG1val = np.append(np.insert(totyPGs[:], 0, PG1bound), PG1bound)
    yPG2val = np.append(np.insert(yPGs[n, :], 0, PG2bound), PG2bound)
    dyPG1val = np.append(np.insert(totyPGerrs[:], 0, PG1errbound), PG1errbound)
    dyPG2val = np.append(np.insert(yPGerrs[n, :], 0, PG2errbound), PG2errbound)
    ax[n].plot(xval, yPG1val, color = [0.5, 0., 0.5], clip_on = False, zorder = 6)
    ax[n].plot(xval, yPG1val - dyPG1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 4)
    ax[n].plot(xval, yPG1val + dyPG1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 4)
    if SSTs[n] >= 0.9:
        col = [1., 0., 0.]
    elif SSTs[n] <= -0.9:
        col = [0., 0., 1.]
    else:
        col = [0., 0.5, 0.]
    if PGNs[n] != 0:
        ax[n].plot(xval, yPG2val, color = col, clip_on = False, zorder = 7)
        ax[n].plot(xval, yPG2val - dyPG2val, color = col, linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(xval, yPG2val + dyPG2val, color = col, linestyle = 'dotted', clip_on = False, zorder = 5)
    ax[n].set_title('Vostok, {0:s}/{1:s} ({2:s}{3:.2f} °C) vs mean'.\
        format(str(2006 + n), str(2007 + n)[2:], sign(SSTs[n]), abs(SSTs[n])), fontsize = 'large')
    ax[n].text(7./24, 1./12, 'Number of days: {0:d}, {1:d}'.\
        format(PGNs[n], totPGN), fontsize = 'large',\
        ha = 'left', va = 'center', transform = ax[n].transAxes)
    if j == 0:
        ax[n].text(-0.29, 1 + 0.07 * 1.6 / h[i], chr(ord('a') + n), fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
    else:
        ax[n].text(-0.15, 1 + 0.07 * 1.6 / h[i], chr(ord('a') + n), fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)

plt.savefig('all_years_year_to_year_removed_5_months.eps', bbox_inches = 'tight')

nw, nh = 2, 4   # number of subplots in horizontal (width) and vertical (height) directions
w = 3.3         # width of each subplot
wt = 0.3        # width of tick labels
wl = 0.5        # width of each axis label
ws = 0.4        # horizontal spacing between subplots
h = [2.0, 2.0, 2.0, 2.0]
# heights of subplots in different rows
assert len(h) == nh
ht = 0.2        # height of tick labels
hl = 0.2        # height of each axis label
hs = 0.2        # vertical spacing between subplots
hu = 0.2        # height of each subplot label
wm = 0.2        # width of margins
tw = nw * (w + wt + wl) + (nw - 1) * ws + 2 * wm        # total width
hm = 0.2        # height of margins
th = sum(h[i] for i in range(nh)) + nh * (ht + hu) + hl + (nh - 1) * hs + 2 * hm        # total height
fig = plt.figure(figsize = (tw, th))
ax = [None for _ in range(nw * nh)]
idx = [3, 9, 4, 5]
for n in range(nw * nh):
    i, j = divmod(n, nw)
    ax[n] = fig.add_axes([(wm + j * (w + ws) + (j + 1) * (wt + wl)) / tw,\
        (hm + hl + sum(h[nh - 1 - _] for _ in range(nh - 1 - i)) + (nh - 1 - i) * (hu + hs) + (nh - i) * ht) / th,\
        w / tw, h[i] / th])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    ax[n].set_xlim((0, 24))
    ax[n].set_xticks(np.arange(0, 25, 2))
    ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
    if i == nh - 1:
        ax[n].set_xlabel('UTC, hours', fontsize = 'large')
    ax[n].get_yaxis().set_label_coords(-0.25, 0.5)
    if j == 0:
        ax[n].set_ylim((0.7, 1.3))
        ax[n].set_yticks(np.arange(0.7, 1.31, 0.1))
        ax[n].set_yticklabels(['0.7', '0.8', '0.9', '1.0', '1.1', '1.2', '1.3'], fontsize = 'large')
        ax[n].set_ylabel('Pot. gradient in Oct–Feb\nas a fraction of the mean', fontsize = 'large', va = 'top')
    else:
        ax[n].set_ylim((0.8, 1.2))
        ax[n].set_yticks(np.arange(0.8, 1.21, 0.1))
        ax[n].set_yticklabels(['0.8', '0.9', '1.0', '1.1', '1.2'], fontsize = 'large')
        ax[n].set_ylabel('Ion. potential in Oct–Feb\nas a fraction of the mean', fontsize = 'large', va = 'top')
    x1val = np.arange(25)
    x2val = np.append(np.insert(np.arange(0.5, 24, 1), 0, 0), 24)
    m = idx[n // 2]
    if SSTs[m] >= 0.9:
        col = [1., 0., 0.]
    elif SSTs[m] <= -0.9:
        col = [0., 0., 1.]
    else:
        col = [0., 0.5, 0.]
    if n % 2 == 0:
        PG1bound = (totyPGs[0] + totyPGs[-1]) / 2
        PG2bound = (yPGs[m, 0] + yPGs[m, -1]) / 2
        PG1errbound = (totyPGerrs[0] + totyPGerrs[-1]) / 2
        PG2errbound = (yPGerrs[m, 0] + yPGerrs[m, -1]) / 2
        yPG1val = np.append(np.insert(totyPGs[:], 0, PG1bound), PG1bound)
        yPG2val = np.append(np.insert(yPGs[m, :], 0, PG2bound), PG2bound)
        dyPG1val = np.append(np.insert(totyPGerrs[:], 0, PG1errbound), PG1errbound)
        dyPG2val = np.append(np.insert(yPGerrs[m, :], 0, PG2errbound), PG2errbound)
        ax[n].plot(x2val, yPG1val, color = [0.5, 0., 0.5], clip_on = False, zorder = 6)
        ax[n].plot(x2val, yPG1val - dyPG1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].plot(x2val, yPG1val + dyPG1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 4)
        if PGNs[n] != 0:
            ax[n].plot(x2val, yPG2val, color = col, clip_on = False, zorder = 7)
            ax[n].plot(x2val, yPG2val - dyPG2val, color = col, linestyle = 'dotted', clip_on = False, zorder = 5)
            ax[n].plot(x2val, yPG2val + dyPG2val, color = col, linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].set_title('Vostok, {0:s}/{1:s} ({2:s}{3:.2f} °C) vs mean'.\
            format(str(2006 + m), str(2007 + m)[2:], sign(SSTs[m]), abs(SSTs[m])), fontsize = 'large')
        ax[n].text(7./24, 1./12, 'Number of days: {0:d}, {1:d}'.\
            format(PGNs[m], totPGN), fontsize = 'large',\
            ha = 'left', va = 'center', transform = ax[n].transAxes)
    else:
        yIP1val = totyIPs[:]
        yIP2val = yIPs[m, :]
        dyIP1val = totyIPerrs[:]
        dyIP2val = yIPerrs[m, :]
        ax[n].plot(x1val, yIP1val, color = [0.5, 0., 0.5], clip_on = False, zorder = 6)
        ax[n].plot(x1val, yIP1val - dyIP1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].plot(x1val, yIP1val + dyIP1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].plot(x1val, yIP2val, color = col, clip_on = False, zorder = 7)
        ax[n].plot(x1val, yIP2val - dyIP2val, color = col, linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x1val, yIP2val + dyIP2val, color = col, linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].set_title('Model, {0:s}/{1:s} ({2:s}{3:.2f} °C) vs mean'.\
            format(str(2006 + m), str(2007 + m)[2:], sign(SSTs[m]), abs(SSTs[m])), fontsize = 'large')
    ax[n].text(-0.27, 1 + 0.07 * 1.6 / h[i], chr(ord('a') + i + 4 * j),\
        fontsize = 'x-large', fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)

plt.savefig('selected_years_year_to_year_removed_5_months.eps', bbox_inches = 'tight')

nw, nh = 2, 3   # number of subplots in horizontal (width) and vertical (height) directions
w = 3.3         # width of each subplot
wt = 0.4        # width of tick labels
wl = 0.4        # width of each axis label
ws = 0.6        # horizontal spacing between subplots
h = [2.0, 2.0, 2.0] # heights of subplots in different rows
assert len(h) == nh
ht = 0.2        # height of tick labels
hl = 0.2        # height of each axis label
hs = 0.2        # vertical spacing between subplots
hu = 0.2        # height of each subplot label
wm = 0.2        # width of margins
tw = nw * (w + wt) + wl + (nw - 1) * ws + 2 * wm        # total width
hm = 0.2        # height of margins
th = sum(h[i] for i in range(nh)) + nh * (ht + hu) + hl + (nh - 1) * hs + 2 * hm        # total height
fig = plt.figure(figsize = (tw, th))
ax = [None for _ in range(nw * nh)]
for n in range(nw * nh):
    i, j = divmod(n, nw)
    ax[n] = fig.add_axes([(wm + wl + j * (w + ws) + (j + 1) * wt) / tw,\
        (hm + hl + sum(h[nh - 1 - _] for _ in range(nh - 1 - i)) + (nh - 1 - i) * (hu + hs) + (nh - i) * ht) / th,\
        w / tw, h[i] / th])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    ax[n].set_xlim((0, 24))
    ax[n].set_xticks(np.arange(0, 25, 2))
    ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
    ax[n].get_yaxis().set_label_coords(-0.25, 0.5)
    if i == 0:
        ax[n].set_ylim((0.7, 1.3))
        ax[n].set_yticks(np.arange(0.7, 1.31, 0.1))
        ax[n].set_yticklabels(['0.7', '0.8', '0.9', '1.0', '1.1', '1.2', '1.3'], fontsize = 'large')
        if j == 0:
            ax[n].set_ylabel('Pot. gradient in Oct–Feb\nas a fraction of the mean', fontsize = 'large', va = 'top')
    elif i == 1:
        ax[n].set_ylim((0.8, 1.2))
        ax[n].set_yticks(np.arange(0.8, 1.21, 0.1))
        ax[n].set_yticklabels(['0.8', '0.9', '1.0', '1.1', '1.2'], fontsize = 'large')
        if j == 0:
            ax[n].set_ylabel('Ion. potential in Oct–Feb\nas a fraction of the mean', fontsize = 'large', va = 'top')
    else:
        ax[n].set_xlabel('UTC, hours', fontsize = 'large')
        ax[n].set_ylim((-0.06, 0.06))
        ax[n].set_yticks(np.arange(-0.06, 0.061, 0.03))
        ax[n].set_yticklabels(['−0.06', '−0.03', '0.00', '+0.03', '+0.06'], fontsize = 'large')
        if j == 0:
            ax[n].set_ylabel('Difference (pos. − neg.)', fontsize = 'large', va = 'top')
    x1val = np.arange(25)
    x2val = np.append(np.insert(np.arange(0.5, 24, 1), 0, 0), 24)
    x3val = np.arange(1, 24, 2)
    if n == 0:
        PG1bound = (posyPGs[0] + posyPGs[-1]) / 2
        PG2bound = (negyPGs[0] + negyPGs[-1]) / 2
        PG1errbound = (posyPGerrs[0] + posyPGerrs[-1]) / 2
        PG2errbound = (negyPGerrs[0] + negyPGerrs[-1]) / 2
        yPG1val = np.append(np.insert(posyPGs[:], 0, PG1bound), PG1bound)
        yPG2val = np.append(np.insert(negyPGs[:], 0, PG2bound), PG2bound)
        dyPG1val = np.append(np.insert(posyPGerrs[:], 0, PG1errbound), PG1errbound)
        dyPG2val = np.append(np.insert(negyPGerrs[:], 0, PG2errbound), PG2errbound)
        ax[n].plot(x2val, yPG1val, color = [1., 0., 0.], clip_on = False, zorder = 7)
        ax[n].plot(x2val, yPG1val - dyPG1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x2val, yPG1val + dyPG1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x2val, yPG2val, color = [0., 0., 1.], clip_on = False, zorder = 6)
        ax[n].plot(x2val, yPG2val - dyPG2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].plot(x2val, yPG2val + dyPG2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].set_title('Vostok, pos. vs neg. SST anomalies', fontsize = 'large')
        ax[n].text(7./24, 1./12, 'Number of days: {0:d}, {1:d}'.\
            format(posPGN, negPGN), fontsize = 'large',\
            ha = 'left', va = 'center', transform = ax[n].transAxes)
    elif n == 1:
        PG1bound = (posyPGs2[0] + posyPGs2[-1]) / 2
        PG2bound = (negyPGs2[0] + negyPGs2[-1]) / 2
        PG1errbound = (posyPGerrs2[0] + posyPGerrs2[-1]) / 2
        PG2errbound = (negyPGerrs2[0] + negyPGerrs2[-1]) / 2
        yPG1val = np.append(np.insert(posyPGs2[:], 0, PG1bound), PG1bound)
        yPG2val = np.append(np.insert(negyPGs2[:], 0, PG2bound), PG2bound)
        dyPG1val = np.append(np.insert(posyPGerrs2[:], 0, PG1errbound), PG1errbound)
        dyPG2val = np.append(np.insert(negyPGerrs2[:], 0, PG2errbound), PG2errbound)
        ax[n].plot(x2val, yPG1val, color = [1., 0., 0.], clip_on = False, zorder = 7)
        ax[n].plot(x2val, yPG1val - dyPG1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x2val, yPG1val + dyPG1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x2val, yPG2val, color = [0., 0., 1.], clip_on = False, zorder = 6)
        ax[n].plot(x2val, yPG2val - dyPG2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].plot(x2val, yPG2val + dyPG2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].set_title('Vostok, strong El Niños vs La Niñas', fontsize = 'large')
        ax[n].text(7./24, 1./12, 'Number of days: {0:d}, {1:d}'.\
            format(posPGN2, negPGN2), fontsize = 'large',\
            ha = 'left', va = 'center', transform = ax[n].transAxes)
    elif n == 2:
        yIP1val = posyIPs[:]
        yIP2val = negyIPs[:]
        dyIP1val = posyIPerrs[:]
        dyIP2val = negyIPerrs[:]
        ax[n].plot(x1val, yIP1val, color = [1., 0., 0.], clip_on = False, zorder = 7)
        ax[n].plot(x1val, yIP1val - dyIP1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x1val, yIP1val + dyIP1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x1val, yIP2val, color = [0., 0., 1.], clip_on = False, zorder = 6)
        ax[n].plot(x1val, yIP2val - dyIP2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].plot(x1val, yIP2val + dyIP2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].set_title('Model, pos. vs neg. SST anomalies', fontsize = 'large')
    elif n == 3:
        yIP1val = posyIPs2[:]
        yIP2val = negyIPs2[:]
        dyIP1val = posyIPerrs2[:]
        dyIP2val = negyIPerrs2[:]
        ax[n].plot(x1val, yIP1val, color = [1., 0., 0.], clip_on = False, zorder = 7)
        ax[n].plot(x1val, yIP1val - dyIP1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x1val, yIP1val + dyIP1val, color = [1., 0., 0.], linestyle = 'dotted', clip_on = False, zorder = 5)
        ax[n].plot(x1val, yIP2val, color = [0., 0., 1.], clip_on = False, zorder = 6)
        ax[n].plot(x1val, yIP2val - dyIP2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].plot(x1val, yIP2val + dyIP2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
        ax[n].set_title('Model, strong El Niños vs La Niñas', fontsize = 'large')
    elif n == 4:
        y1val = (posyPGs[::2] + posyPGs[1::2] - negyPGs[::2] - negyPGs[1::2]) / 2
        y2val = (posyIPs[:-2:2] + 2 * posyIPs[1:-1:2] + posyIPs[2::2] -\
            negyIPs[:-2:2] - 2 * negyIPs[1:-1:2] - negyIPs[2::2]) / 4
        ax[n].bar(x3val - 0.4, y1val, 0.8, color = [0.5, 0., 0.5])
        ax[n].bar(x3val + 0.4, y2val, 0.8, color = [1., 0.5, 0.])
        # corr = round(np.corrcoef(y1val, y2val)[0, 1], 2)
        # if corr >= 0:
            # ax[n].set_title('Vostok vs model (r = {0:.2f})'.\
                # format(corr), fontsize = 'large')
        # else:
            # ax[n].set_title('Vostok vs model (r = −{0:.2f})'.\
                # format(-corr), fontsize = 'large')
        ax[n].set_title('Vostok vs model', fontsize = 'large')
    else:
        y1val = (posyPGs2[::2] + posyPGs2[1::2] - negyPGs2[::2] - negyPGs2[1::2]) / 2
        y2val = (posyIPs2[:-2:2] + 2 * posyIPs2[1:-1:2] + posyIPs2[2::2] -\
            negyIPs2[:-2:2] - 2 * negyIPs2[1:-1:2] - negyIPs2[2::2]) / 4
        ax[n].bar(x3val - 0.4, y1val, 0.8, color = [0.5, 0., 0.5])
        ax[n].bar(x3val + 0.4, y2val, 0.8, color = [1., 0.5, 0.])
        # corr = np.corrcoef(y1val, y2val)[0, 1]
        # if corr >= 0:
            # ax[n].set_title('Vostok vs model (r = {0:.2f})'.\
                # format(corr), fontsize = 'large')
        # else:
            # ax[n].set_title('Vostok vs model (r = −{0:.2f})'.\
                # format(-corr), fontsize = 'large')
        ax[n].set_title('Vostok vs model', fontsize = 'large')
    if j == 0:
        ax[n].text(-0.27, 1 + 0.07 * 1.6 / h[i], chr(ord('a') + n), fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
    else:
        ax[n].text(-0.21, 1 + 0.07 * 1.6 / h[i], chr(ord('a') + n), fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)

plt.savefig('ENSO_year_to_year_removed_5_months.eps', bbox_inches = 'tight')
